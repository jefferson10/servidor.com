<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::controller('cadastro','ControladorCadastros');
Route::controller('login','ControladorLogin');
Route::controller('busca','ControladorBuscas');
Route::controller('vistoria','ControladorVistoria');  
Route::controller('edit','ControladorEdicao');  
Route::get('/', function()
{
   $user = User::all();
   $imob = Imobiliaria::all();
    return $user.$imob;
//	return View::make('home.index');
    
});


