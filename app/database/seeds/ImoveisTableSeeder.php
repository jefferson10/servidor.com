<?php

class ImoveisTableSeeder extends Seeder {

    public function run() {

        DB::table('imoveis')->delete();

        Imovel::create(array(
            'imob_id' => '1',
            'rua' => 'Olinto',
            'numero' => '456',
            'bairro' => 'centro',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
        Imovel::create(array(
            'imob_id' => '1',
            'rua' => 'Alagoas',
            'numero' => '543',
            'bairro' => 'centro',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
        Imovel::create(array(
            'imob_id' => '1',
            'rua' => 'Argentina',
            'numero' => '456',
            'bairro' => 'quissisana',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
        Imovel::create(array(
            'imob_id' => '2',
            'rua' => 'Pompilio Merli',
            'numero' => '765',
            'bairro' => 'Vila Guaporé',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
        Imovel::create(array(
            'imob_id' => '2',
            'rua' => 'Nico Duarte',
            'numero' => '12',
            'bairro' => 'Vila Cruz',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
        Imovel::create(array(
            'imob_id' => '2',
            'rua' => 'Eliglio Eberle',
            'numero' => '8978',
            'bairro' => 'Santa Helena',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
        Imovel::create(array(
            'imob_id' => '3',
            'rua' => 'Lombard Moura',
            'numero' => '45',
            'bairro' => 'Vila Neuza',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
        Imovel::create(array(
            'imob_id' => '3',
            'rua' => 'Assis de Figueiredo',
            'numero' => '1111',
            'bairro' => 'Centro',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
        Imovel::create(array(
            'imob_id' => '3',
            'rua' => 'Correa Neto',
            'numero' => '564',
            'bairro' => 'Centro',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
        ));
    }

}
