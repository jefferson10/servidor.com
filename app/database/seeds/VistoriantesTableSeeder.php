<?php

class VistoriantesTableSeeder extends Seeder {

    public function run() {

        DB::table('vistoriantes')->delete();

        Vistoriante::create(array(
            'imob_id' => '1',
            'nomeVist' => 'joao augusto',
            'cpf' => '192.345.291-60',
            'rua' => 'Amazonas',
            'numero' => '43',
            'bairro' => 'Vila Cruz',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
            'fone' => '(35) 3714-3476',
            'email' => 'joaoaugusto@gmail.com',
            'password' => Hash::make('123456'),
            'dataCadastro' => date('Y,m,d'),
            'ativo' => '1',
        ));

        Vistoriante::create(array(
            'imob_id' => '1',
            'nomeVist' => 'caio cezar',
            'cpf' => '153.905.261-60',
            'rua' => 'Nico Duarte',
            'numero' => '653',
            'bairro' => 'Vila Cruz',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
            'fone' => '(35) 8814-9576',
            'email' => 'caiocezar@gmail.com',
            'password' => Hash::make('123456'),
            'dataCadastro' => date('Y,m,d'),
            'ativo' => '1',
        ));

        Vistoriante::create(array(
            'imob_id' => '2',
            'nomeVist' => 'roberto silva',
            'cpf' => '092.657.534-63',
            'rua' => 'Argentina',
            'numero' => '67',
            'bairro' => 'Quissizana',
            'cidade' => 'Poços de Caldas',
            'fone' => '(35) 9874-8932',
            'email' => 'robertosilva@gmail.com',
            'password' => Hash::make('123456'),
            'dataCadastro' => date('Y,m,d'),
            'ativo' => '1',
        ));

        Vistoriante::create(array(
            'imob_id' => '2',
            'nomeVist' => 'higor pena',
            'cpf' => '065.366.987-60',
            'rua' => 'Pinangua',
            'numero' => '543',
            'bairro' => 'Vila Togni',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
            'fone' => '(35) 3722-7768',
            'email' => 'higorpena@gmail.com',
            'password' => Hash::make('123456'),
            'dataCadastro' => date('Y,m,d'),
            'ativo' => '1',
        ));
        Vistoriante::create(array(
            'imob_id' => '3',
            'nomeVist' => 'rafael olencar',
            'cpf' => '197.234.654-40',
            'rua' => 'Chile',
            'numero' => '195',
            'bairro' => 'Jd Centenário',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
            'fone' => '(35) 3722-6726',
            'email' => 'rafaelolencar@gmail.com',
            'password' => Hash::make('123456'),
            'dataCadastro' => date('Y,m,d'),
            'ativo' => '1',
        ));

        Vistoriante::create(array(
            'imob_id' => '3',
            'nomeVist' => 'paulo henrique',
            'cpf' => '186.767.353-20',
            'rua' => 'Pompili Crimas',
            'numero' => '980',
            'bairro' => 'Bortolan',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
            'fone' => '(35) 8865-7239',
            'email' => 'paulohenrique@gmail.com',
            'password' => Hash::make('123456'),
            'dataCadastro' => date('Y,m,d'),
            'ativo' => '1',
        ));

        Vistoriante::create(array(
            'imob_id' => '0',
            'nomeVist' => 'augusto juliano',
            'cpf' => '296.767.393-27',
            'rua' => 'Brasil',
            'numero' => '80',
            'bairro' => 'Centro',
            'cidade' => 'Poços de Caldas',
            'estado' => 'MG',
            'fone' => '(35) 9954-7239',
            'email' => 'augustojuliano@gmail.com',
            'password' => Hash::make('123456'),
            'dataCadastro' => date('Y,m,d'),
            'ativo' => '1',
        ));
    }

}
