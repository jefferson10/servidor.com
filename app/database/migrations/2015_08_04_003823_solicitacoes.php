<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Solicitacoes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
 Schema::create('solicitacoes', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('imob_id');
            $table->integer('imov_id');
            $table->integer('vist_id');
            $table->date('dataSolicitacao');
            $table->boolean('situacao')->default(FALSE);
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solicitacoes');
	}

}
