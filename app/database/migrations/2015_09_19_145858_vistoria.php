<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vistoria extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('vistorias', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('id_solicitacao');
            $table->integer('nota');
            $table->integer('portas');
            $table->integer('janelas');
            $table->integer('fechaduras');
            $table->integer('tomadas');
            $table->integer('pintura');
            $table->integer('pisos');
            $table->integer('torneiras');
            $table->integer('luminarias');
            $table->integer('pia');
            $table->integer('tanque');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('vistorias');
    }

}
