<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vistoriantes extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('vistoriantes', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('imob_id');
            $table->string('nomeVist', 30);
            $table->string('cpf', 14)->unique();
            $table->string('rua', 50);
            $table->integer('numero');
            $table->string('complemento', 20)->nullable()->default('nenhum');
            $table->string('bairro', 50);
            $table->string('cidade', 50);
            $table->string('estado', 3);
            $table->string('fone', 15);
            $table->string('email', 100)->unique();
            $table->string('password', 60);
            $table->date('dataCadastro');
            $table->integer('notas');
            $table->integer('qtd_vistorias');
            $table->boolean('ativo')->default(TRUE);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('vistoriantes');
    }

}
