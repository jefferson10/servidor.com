<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Imobiliarias extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('imobiliarias', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->string('nomeImob', 30);
            $table->string('cnpj', 18)->unique();
            $table->string('rua', 50);
            $table->integer('numero');
            $table->string('complemento', 20)->nullable()->default('nenhum');
            $table->string('bairro', 50);
            $table->string('cidade', 50);
            $table->string('estado', 3);
            $table->string('fone', 15);
            $table->string('creci', 6);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('imobiliarias');
    }

}
