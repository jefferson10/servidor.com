<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function(Blueprint $table) {

            $table->increments('id');
            $table->string('email', 100)->unique();
            $table->string('password', 60);
            $table->date('dataCadastro');
            $table->integer('perfil');
            $table->boolean('ativo')->default(TRUE);
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }

}
