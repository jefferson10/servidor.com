<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Imoveis extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('imoveis', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('imob_id');
            $table->string('rua', 50);
            $table->integer('numero');
            $table->string('complemento', 20)->nullable()->default('nenhum');
            $table->string('bairro', 50);
            $table->string('cidade', 50);
            $table->string('estado', 3);
            $table->boolean('solicitacao')->default(FALSE);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('imoveis');
    }

}
