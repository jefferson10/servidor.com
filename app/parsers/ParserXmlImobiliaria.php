<?php

class ParserXmlImobiliaria {

    public $codificacao = "UTF-8";
    public function desmontaXmlImobiliariaCadastro($xml) {

        //retira os espaÃ§os da string para que a mesma possa ser serealizada para SimpleXml
        $xmlFormatado = str_replace("\n", "", $xml);
        //monta um new Simplexml de uma string
        return simplexml_load_string($xmlFormatado);
    }
    
    //monta um xml de retorno, com o resultado da requisiÃ§Ã£o. 1 = true e 0 = false
    public function montaXmlRetornoCadastroImobiliaria($result){
       
        $dom = new DOMDocument("1.0", $this->codificacao);
        #retirar os espacos em branco
        $dom->preserveWhiteSpace = false;
        #gerar o codigo
        $dom->formatOutput = true;
        #criando o nÃ³ principal (root)
        $retornoImobiliaria = $dom->createElement("returnCadastroImobiliaria");
        #nÃ³ filho (usuario)
        $retornoCadastroImobiliaria = $dom->createElement("ImobiliariaCadastro");
        #setanto nomes e atributos dos elementos xml (nÃ³s)
        $resultado = $dom->createElement("resultado", $result); 
        $retornoCadastroImobiliaria->appendChild($resultado);
        $retornoImobiliaria->appendChild($retornoCadastroImobiliaria);
        $dom->appendChild($retornoImobiliaria);
            
        $xml = $dom->saveXML();
       //echo $dom->saveXML();
        return $xml;               
    }
}