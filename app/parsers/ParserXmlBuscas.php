<?php

class ParserXmlBuscas {
    public $codificacao = "UTF-8";
    
////////////////////// IMOVEL /////////////////////////////////
    //monta xml retorno da busca pelos imoveis
    public function montaXmlRetornoBuscaImoveis($imoveis, $resultado) {
        if ($imoveis != null) {
            $dom = new DOMDocument("1.0",$this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaImovel = $dom->createElement("returnBuscaImovel");
            #nÃ³ filho (usuario)
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $retornoBuscaImovel->appendChild($result);
            foreach ($imoveis as $imovel) {
                $imoveisCreate = $dom->createElement("imovel");
                $id = $dom->createElement("id", $imovel['id']);
                $rua = $dom->createElement("rua", $imovel['rua'] . " " . $imovel['numero']);
                $imoveisCreate->appendChild($id);
                $imoveisCreate->appendChild($rua);
                $retornoBuscaImovel->appendChild($imoveisCreate);
            }


            $dom->appendChild($retornoBuscaImovel);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaImovel = $dom->createElement("returnBuscaImovel");
            #nÃ³ filho (usuario)
            $imoveisCreate = $dom->createElement("imovel");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $id = $dom->createElement("id", '');
            $rua = $dom->createElement("rua", '');
            $imoveisCreate->appendChild($id);
            $imoveisCreate->appendChild($rua);

            $retornoBuscaImovel->appendChild($result);
            $retornoBuscaImovel->appendChild($imoveisCreate);
            $dom->appendChild($retornoBuscaImovel);

            $xml = $dom->saveXML();
            return $xml;
        }
    }

    //monta xml retorno da busca pelos imovel
    public function montaXmlRetornoBuscaImovel($imovel) {
        if ($imovel != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaImovel = $dom->createElement("returnBuscaImovel");
            #nÃ³ filho (usuario)
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '1');
            $retornoBuscaImovel->appendChild($result);

            $imovelCreate = $dom->createElement("imovel");

            $rua = $dom->createElement("rua", $imovel['rua']);
            $numero = $dom->createElement("numero", $imovel['numero']);
            $complemento = $dom->createElement("complemento", $imovel['complemento']);
            $bairro = $dom->createElement("bairro", $imovel['bairro']);
            $cidade = $dom->createElement("cidade", $imovel['cidade']);
            $estado = $dom->createElement("estado", $imovel['estado']);
            $imovelCreate->appendChild($rua);
            $imovelCreate->appendChild($numero);
            $imovelCreate->appendChild($complemento);
            $imovelCreate->appendChild($bairro);
            $imovelCreate->appendChild($cidade);
            $imovelCreate->appendChild($estado);
            $retornoBuscaImovel->appendChild($imovelCreate);
            $dom->appendChild($retornoBuscaImovel);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaImovel = $dom->createElement("returnBuscaImovel");
            #nÃ³ filho (usuario)
            $imovelCreate = $dom->createElement("imovel");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '0');
            $rua = $dom->createElement("rua", '');
            $numero = $dom->createElement("numero", '');
            $complemento = $dom->createElement("complemento", '');
            $bairro = $dom->createElement("bairro", '');
            $cidade = $dom->createElement("cidade", '');
            $estado = $dom->createElement("estado", '');
            $imovelCreate->appendChild($rua);
            $imovelCreate->appendChild($numero);
            $imovelCreate->appendChild($complemento);
            $imovelCreate->appendChild($bairro);
            $imovelCreate->appendChild($cidade);
            $imovelCreate->appendChild($estado);
            $retornoBuscaImovel->appendChild($result);
            $retornoBuscaImovel->appendChild($imovelCreate);
            $dom->appendChild($retornoBuscaImovel);

            $xml = $dom->saveXML();
            return $xml;
        }
    }

////////////////////// VISTORIAN-TE /////////////////////////////////
    //monta xml retorno da busca pelos vistoriantes
    public function montaXmlRetornoBuscaVistoriantes($vistoriantes) {
        if ($vistoriantes != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaVistoriante = $dom->createElement("returnBuscaVistoriante");
            #nÃ³ filho (usuario)
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '1');
            $retornoBuscaVistoriante->appendChild($result);
            foreach ($vistoriantes as $vistoriante) {
                $vistorianteCreate = $dom->createElement("vistoriante");
                $id = $dom->createElement("id", $vistoriante['id']);
                $nome = $dom->createElement("nomeVist", $vistoriante['nomeVist']);
                $vistorianteCreate->appendChild($id);
                $vistorianteCreate->appendChild($nome);
                $retornoBuscaVistoriante->appendChild($vistorianteCreate);
            }


            $dom->appendChild($retornoBuscaVistoriante);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaVistoriante = $dom->createElement("returnBuscaVistoriante");
            #nÃ³ filho (usuario)
            $vistorianteCreate = $dom->createElement("vistoriante");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '0');
            $id = $dom->createElement("id", '');
            $nome = $dom->createElement("nomeVist", '');
            $vistorianteCreate->appendChild($id);
            $vistorianteCreate->appendChild($nome);

            $retornoBuscaVistoriante->appendChild($result);
            $retornoBuscaVistoriante->appendChild($vistorianteCreate);
            $dom->appendChild($retornoBuscaVistoriante);

            $xml = $dom->saveXML();
            return $xml;
        }
    }

    //monta xml retorno da busca pelos vistoriante
    public function montaXmlRetornoBuscaVistoriante($vistoriante) {
        if ($vistoriante != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaVistoriante = $dom->createElement("returnBuscaVistoriante");
            #nÃ³ filho (usuario)
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '1');
            $retornoBuscaVistoriante->appendChild($result);

            $vistorianteCreate = $dom->createElement("vistoriante");

            $cpf = $dom->createElement("cpf", $vistoriante['cpf']);
            $nomeVist = $dom->createElement("nomeVist", $vistoriante['nomeVist']);
            $nota = $dom->createElement("notas", $vistoriante['notas']);
            $vistorianteCreate->appendChild($cpf);
            $vistorianteCreate->appendChild($nomeVist);
            $vistorianteCreate->appendChild($nota);
            $retornoBuscaVistoriante->appendChild($vistorianteCreate);
            $dom->appendChild($retornoBuscaVistoriante);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaVistoriante = $dom->createElement("returnBuscaVistoriante");
            #nÃ³ filho (usuario)
            $vistorianteCreate = $dom->createElement("vistoriante");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '0');
            $cpf = $dom->createElement("cpf", '');
            $nomeVist = $dom->createElement("nomeVist", '');
            $nota = $dom->createElement("nota", '');
            $vistorianteCreate->appendChild($cpf);
            $vistorianteCreate->appendChild($nomeVist);
            $vistorianteCreate->appendChild($nota);
            $retornoBuscaVistoriante->appendChild($result);
            $retornoBuscaVistoriante->appendChild($vistorianteCreate);
            $dom->appendChild($retornoBuscaVistoriante);

            $xml = $dom->saveXML();
            return $xml;
        }
    }

    //monta xml retorno da busca por solicitacoes
    public function montaXmlRetornoBuscaSolicitacoes($solicitacoes, $resultado) {
        if ($solicitacoes != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaSolicitacoes = $dom->createElement("returnBuscaSolicitacoes");
            #nÃ³ filho (usuario)
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $retornoBuscaSolicitacoes->appendChild($result);
            foreach ($solicitacoes as $solicitacao) {
                $solici = SolicitacaoVistoria::find($solicitacao->id);
                $imovel = $solici->imovel;
                $vistoriante = $solici->vistoriante;


                $solicitacaoCreate = $dom->createElement("solicitacao");
                $id = $dom->createElement("id", $solici->id);
                $data = $dom->createElement("data", $solici->dataSolicitacao);
                $rua = $dom->createElement("rua", $imovel->rua . " " . $imovel->numero);
                $vistorian = $dom->createElement("nomeVist", $vistoriante->nomeVist);
                $solicitacaoCreate->appendChild($id);
                $solicitacaoCreate->appendChild($rua);
                $solicitacaoCreate->appendChild($data);
                $solicitacaoCreate->appendChild($vistorian);
                $retornoBuscaSolicitacoes->appendChild($solicitacaoCreate);
            }


            $dom->appendChild($retornoBuscaSolicitacoes);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaSolicitacoes = $dom->createElement("returnBuscaSolicitacoes");
            #nÃ³ filho (usuario)
            $solicitacaoCreate = $dom->createElement("vistoriante");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $id = $dom->createElement("id", '');
            $data = $dom->createElement("data", '');
            $rua = $dom->createElement("rua", '');
            $vistorian = $dom->createElement("nomeVist", '');
            $solicitacaoCreate->appendChild($id);
            $solicitacaoCreate->appendChild($rua);
            $solicitacaoCreate->appendChild($data);
            $solicitacaoCreate->appendChild($vistorian);

            $retornoBuscaSolicitacoes->appendChild($result);
            $retornoBuscaSolicitacoes->appendChild($solicitacaoCreate);
            $dom->appendChild($retornoBuscaSolicitacoes);

            $xml = $dom->saveXML();
            return $xml;
        }
    }

    //monta xml retorno da busca pela imobiliaria
    public function montaXmlRetornoBuscaImobiliaria($imobiliaria, $resultado) {
        if ($imobiliaria != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaImobiliaria = $dom->createElement("returnBuscaImobiliaria");
            #nÃ³ filho (usuario)
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '1');
            $retornoBuscaImobiliaria->appendChild($result);

            $imobiliariaCreate = $dom->createElement("imobiliaria");
            $id = $dom->createElement("id", $imobiliaria->id);
            $nomeImob = $dom->createElement("nomeImob", $imobiliaria->nomeImob);
            $cnpj = $dom->createElement("cnpj", $imobiliaria->cnpj);
            $rua = $dom->createElement("rua", $imobiliaria->rua);
            $numero = $dom->createElement("numero", $imobiliaria->numero);
            $complemento = $dom->createElement("complemento", $imobiliaria->complemento);
            $bairro = $dom->createElement("bairro", $imobiliaria->bairro);
            $cidade = $dom->createElement("cidade", $imobiliaria->cidade);
            $estado = $dom->createElement("estado", $imobiliaria->estado);
            $fone = $dom->createElement("fone", $imobiliaria->fone);
            $creci = $dom->createElement("creci", $imobiliaria->creci);
            $imobiliariaCreate->appendChild($id);
            $imobiliariaCreate->appendChild($nomeImob);
            $imobiliariaCreate->appendChild($cnpj);
            $imobiliariaCreate->appendChild($rua);
            $imobiliariaCreate->appendChild($numero);
            $imobiliariaCreate->appendChild($complemento);
            $imobiliariaCreate->appendChild($bairro);
            $imobiliariaCreate->appendChild($cidade);
            $imobiliariaCreate->appendChild($estado);
            $imobiliariaCreate->appendChild($fone);
            $imobiliariaCreate->appendChild($creci);
            $retornoBuscaImobiliaria->appendChild($imobiliariaCreate);
            $dom->appendChild($retornoBuscaImobiliaria);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaImobiliaria = $dom->createElement("returnBuscaImobiliaria");
            #nÃ³ filho (usuario)
            $imobiliariaCreate = $dom->createElement("imobiliaria");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '1');
            $retornoBuscaImobiliaria->appendChild($result);

            $imobiliariaCreate = $dom->createElement("imobiliaria");
            $id = $dom->createElement("id", '');
            $nomeImob = $dom->createElement("nomeImob", '');
            $cnpj = $dom->createElement("cnpj", '');
            $rua = $dom->createElement("rua", '');
            $numero = $dom->createElement("numero", '');
            $complemento = $dom->createElement("complemento", '');
            $bairro = $dom->createElement("bairro", '');
            $cidade = $dom->createElement("cidade", '');
            $estado = $dom->createElement("estado", '');
            $fone = $dom->createElement("fone", '');
            $creci = $dom->createElement("creci", '');
            $imobiliariaCreate->appendChild($id);
            $imobiliariaCreate->appendChild($nomeImob);
            $imobiliariaCreate->appendChild($cnpj);
            $imobiliariaCreate->appendChild($rua);
            $imobiliariaCreate->appendChild($numero);
            $imobiliariaCreate->appendChild($complemento);
            $imobiliariaCreate->appendChild($bairro);
            $imobiliariaCreate->appendChild($cidade);
            $imobiliariaCreate->appendChild($estado);
            $imobiliariaCreate->appendChild($fone);
            $imobiliariaCreate->appendChild($creci);
            $retornoBuscaImobiliaria->appendChild($imobiliariaCreate);
            $dom->appendChild($retornoBuscaImobiliaria);
            $xml = $dom->saveXML();
            return $xml;
        }
    }

    //monta xml retorno da busca por solicitacoes
    public function montaXmlRetornoBuscaSolicitacoesVistoriante($solicitacoes, $resultado) {
        if ($solicitacoes != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaSolicitacoes = $dom->createElement("returnBuscaSolicitacoes");
            #nÃ³ filho (usuario)
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $retornoBuscaSolicitacoes->appendChild($result);
            $listaSolicitacoes = $dom->createElement("solicitacoes");
            foreach ($solicitacoes as $solicitacao) {
                $solici = SolicitacaoVistoria::find($solicitacao->id);
                $imobiliaria = $solici->imobiliaria;

                $solicitacaoCreate = $dom->createElement("solicitacao");
                $id = $dom->createElement("id", $solici->id);
                $data = $dom->createElement("data", $solici->dataSolicitacao);
                $nomeImob = $dom->createElement("nomeImob", $imobiliaria->nomeImob);
                $solicitacaoCreate->appendChild($id);
                $solicitacaoCreate->appendChild($nomeImob);
                $solicitacaoCreate->appendChild($data);
                $listaSolicitacoes->appendChild($solicitacaoCreate);
            }
            $retornoBuscaSolicitacoes->appendChild($listaSolicitacoes);

            $dom->appendChild($retornoBuscaSolicitacoes);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaSolicitacoes = $dom->createElement("returnBuscaSolicitacoes");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $retornoBuscaSolicitacoes->appendChild($result);
            $listaSolicitacoes = $dom->createElement("solicitacoes");

            $solicitacaoCreate = $dom->createElement("solicitacao");
            $id = $dom->createElement("id","");
            $data = $dom->createElement("data","");
            $nomeImob = $dom->createElement("nomeImob","");
            $solicitacaoCreate->appendChild($id);
            $solicitacaoCreate->appendChild($nomeImob);
            $solicitacaoCreate->appendChild($data);
            $listaSolicitacoes->appendChild($solicitacaoCreate);

            $retornoBuscaSolicitacoes->appendChild($listaSolicitacoes);

            $dom->appendChild($retornoBuscaSolicitacoes);

            $xml = $dom->saveXML();
            return $xml;
        }
    }
    
    //monta xml retorno da busca pelos imovel
    public function montaXmlRetornoBuscaImovelVistoriante($imovel,$resultado) {
        if ($imovel != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaImovel = $dom->createElement("returnBuscaImovel");
            #nÃ³ filho (usuario)
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", '1');
            $retornoBuscaImovel->appendChild($result);

            $imovelCreate = $dom->createElement("imovel");

            $rua = $dom->createElement("rua", $imovel->rua);
            $numero = $dom->createElement("numero", $imovel->numero);
            $complemento = $dom->createElement("complemento", $imovel->complemento);
            $bairro = $dom->createElement("bairro", $imovel->bairro);
            $cidade = $dom->createElement("cidade", $imovel->cidade);
            $estado = $dom->createElement("estado", $imovel->estado);
            $imovelCreate->appendChild($rua);
            $imovelCreate->appendChild($numero);
            $imovelCreate->appendChild($complemento);
            $imovelCreate->appendChild($bairro);
            $imovelCreate->appendChild($cidade);
            $imovelCreate->appendChild($estado);
            $retornoBuscaImovel->appendChild($imovelCreate);
            $dom->appendChild($retornoBuscaImovel);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoBuscaImovel = $dom->createElement("returnBuscaImovel");
            #nÃ³ filho (usuario)
            $imovelCreate = $dom->createElement("imovel");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $rua = $dom->createElement("rua", '');
            $numero = $dom->createElement("numero", '');
            $complemento = $dom->createElement("complemento", '');
            $bairro = $dom->createElement("bairro", '');
            $cidade = $dom->createElement("cidade", '');
            $estado = $dom->createElement("estado", '');
            $imovelCreate->appendChild($rua);
            $imovelCreate->appendChild($numero);
            $imovelCreate->appendChild($complemento);
            $imovelCreate->appendChild($bairro);
            $imovelCreate->appendChild($cidade);
            $imovelCreate->appendChild($estado);
            $retornoBuscaImovel->appendChild($result);
            $retornoBuscaImovel->appendChild($imovelCreate);
            $dom->appendChild($retornoBuscaImovel);

            $xml = $dom->saveXML();
            return $xml;
        }
    }

    //desmonta xml de busca
    public function desmontaXmlBusca($xml) {

        //retira os espaÃ§os da string para que a mesma possa ser serealizada para SimpleXml
        $xmlFormatado = str_replace("\n", "", $xml);
        //monta um new Simplexml de uma string
        $xml = simplexml_load_string($xmlFormatado);
        return $xml;
    }

}
