<?php

class ParserXmlSolicitacaoVistoria {
    
    public $codificacao = "UTF-8";
    //monta o xml da requisição de solicitação de vistoria
    public function montaXmlRetornoSoliciatacaoVistoria($resultado) {
        
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $SolicitacaoVistoria = $dom->createElement("SolicitacaoVistoria");
            #nÃ³ filho (usuario)
           
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            
            
                $result = $dom->createElement("result",$resultado);
                        
            $SolicitacaoVistoria->appendChild($result);
            
            $dom->appendChild($SolicitacaoVistoria);

            $xml = $dom->saveXML();
            return $xml;
        
    }
    
    
    //desmonta xml de retorno de busca imovel
    public function desmontaXml($xml) {

        //retira os espaÃ§os da string para que a mesma possa ser serealizada para SimpleXml
        $xmlFormatado = str_replace("\n", "", $xml);
        //monta um new Simplexml de uma string
        $xml = simplexml_load_string($xmlFormatado);
        return $xml;
    }
    
    
    
}
