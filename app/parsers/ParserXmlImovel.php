<?php

class ParserXmlImovel {
    public $codificacao = "UTF-8";
    public function desmontaXmlImovelCadastro($xml) {

        //retira os espaÃ§os da string para que a mesma possa ser serealizada para SimpleXml
        $xmlFormatado = str_replace("\n", "", $xml);
        //monta um new Simplexml de uma string
        return simplexml_load_string($xmlFormatado);
    }
    
    //monta um xml de retorno, com o resultado da requisiÃ§Ã£o. 1 = true e 0 = false
    public function montaXmlRetornoCadastroImovel($result){
       
        $dom = new DOMDocument("1.0", $this->codificacao);
        #retirar os espacos em branco
        $dom->preserveWhiteSpace = false;
        #gerar o codigo
        $dom->formatOutput = true;
        #criando o nÃ³ principal (root)
        $retornoImovel = $dom->createElement("returnCadastroImovel");
        #nÃ³ filho (usuario)
        $retornoCadastroImovel = $dom->createElement("ImovelCadastro");
        #setanto nomes e atributos dos elementos xml (nÃ³s)
        $resultado = $dom->createElement("resultado", $result); 
        $retornoCadastroImovel->appendChild($resultado);
        $retornoImovel->appendChild($retornoCadastroImovel);
        $dom->appendChild($retornoImovel);
            
        $xml = $dom->saveXML();
       //echo $dom->saveXML();
        return $xml;               
    }

    

}
