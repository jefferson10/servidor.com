<?php

class ParserXmlLogin {
    public $codificacao = "UTF-8";
    public function montaXmlRetornoLogin($user, $resultado) {
        if ($user != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoLogin = $dom->createElement("returnLogin");
            #nÃ³ filho (usuario)
            $usuarioCreate = $dom->createElement("usuarioLog");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $id = $dom->createElement("id", $user->id);
            $senha = $dom->createElement("password", Crypt::encrypt($user->password));
            $email = $dom->createElement("email", $user->email);
            $dataCadastro = $dom->createElement("dataCadastro", $user->dataCadastro);
            $perfil = $dom->createElement("perfil", $user->perfil);
            $ativo = $dom->createElement("ativo", $user->ativo);
            $remember_token = $dom->createElement("remember_token", $user->remember_token);

            $usuarioCreate->appendChild($result);
            $usuarioCreate->appendChild($id);
            $usuarioCreate->appendChild($senha);
            $usuarioCreate->appendChild($email);
            $usuarioCreate->appendChild($dataCadastro);
            $usuarioCreate->appendChild($perfil);
            $usuarioCreate->appendChild($ativo);
            $usuarioCreate->appendChild($remember_token);

            $retornoLogin->appendChild($usuarioCreate);
            $dom->appendChild($retornoLogin);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoLogin = $dom->createElement("returnLogin");
            #nÃ³ filho (usuario)
            $usuarioCreate = $dom->createElement("usuarioLog");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $id = $dom->createElement("id", " ");
            $senha = $dom->createElement("password", " ");
            $email = $dom->createElement("email", " ");
            $dataCadastro = $dom->createElement("dataCadastro", " ");
            $perfil = $dom->createElement("perfil", " ");
            $ativo = $dom->createElement("ativo", " ");
            $remember_token = $dom->createElement("remember_token", " ");

            $usuarioCreate->appendChild($result);
            $usuarioCreate->appendChild($id);
            $usuarioCreate->appendChild($senha);
            $usuarioCreate->appendChild($email);
            $usuarioCreate->appendChild($dataCadastro);
            $usuarioCreate->appendChild($perfil);
            $usuarioCreate->appendChild($ativo);
            $usuarioCreate->appendChild($remember_token);

            $retornoLogin->appendChild($usuarioCreate);
            $dom->appendChild($retornoLogin);

            $xml = $dom->saveXML();
            return $xml;
        }
    }
    
    public function montaXmlRetornoLoginVistoriante($vistoriante, $resultado) {
        if ($vistoriante != null) {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoLogin = $dom->createElement("returnLoginVistoriante");
            #nÃ³ filho (usuario)
            $vistorianteCreate = $dom->createElement("vistoriante");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $id = $dom->createElement("id", $vistoriante->id);
            $imob_id = $dom->createElement("imob_id", $vistoriante->imob_id);
            $nomeVist = $dom->createElement("nomeVist", $vistoriante->nomeVist);
            $cpf = $dom->createElement("cpf", $vistoriante->cpf);
            $rua = $dom->createElement("rua", $vistoriante->rua);
            $numero = $dom->createElement("numero", $vistoriante->numero);
            $complemento = $dom->createElement("complemento", $vistoriante->complemento);
            $bairro = $dom->createElement("bairro", $vistoriante->bairro);
            $cidade = $dom->createElement("cidade", $vistoriante->cidade);
            $estado = $dom->createElement("estado", $vistoriante->estado);
            $fone = $dom->createElement("fone", $vistoriante->fone);
            $senha = $dom->createElement("password",  Criptografia::criptografa($vistoriante->password));
            $email = $dom->createElement("email",  Criptografia::criptografa( $vistoriante->email));
            $notas = $dom->createElement("notas", $vistoriante->notas);
            $qtd_vistorias = $dom->createElement("qtd_vistorias", $vistoriante->qtd_vistorias);      
           
            $vistorianteCreate->appendChild($result);
            $vistorianteCreate->appendChild($id);
            $vistorianteCreate->appendChild($imob_id);
            $vistorianteCreate->appendChild($nomeVist);
            $vistorianteCreate->appendChild($cpf);
            $vistorianteCreate->appendChild($rua);
            $vistorianteCreate->appendChild($numero);
            $vistorianteCreate->appendChild($complemento);
            $vistorianteCreate->appendChild($bairro);
            $vistorianteCreate->appendChild($cidade);
            $vistorianteCreate->appendChild($estado);
            $vistorianteCreate->appendChild($fone);
            $vistorianteCreate->appendChild($senha);
            $vistorianteCreate->appendChild($email);
            $vistorianteCreate->appendChild($notas);
            $vistorianteCreate->appendChild($qtd_vistorias);
            
            $retornoLogin->appendChild($vistorianteCreate);
            $dom->appendChild($retornoLogin);

            $xml = $dom->saveXML();
            return $xml;
        } else {
            $dom = new DOMDocument("1.0", $this->codificacao);
            #retirar os espacos em branco
            $dom->preserveWhiteSpace = false;
            #gerar o codigo
            $dom->formatOutput = true;
            #criando o nÃ³ principal (root)
            $retornoLogin = $dom->createElement("returnLogin");
            #nÃ³ filho (usuario)
            $vistorianteCreate = $dom->createElement("vistoriante");
            #setanto nomes e atributos dos elementos xml (nÃ³s)
            $result = $dom->createElement("resultado", $resultado);
            $id = $dom->createElement("id", "");
            $imob_id = $dom->createElement("imob_id", "");
            $nomeVist = $dom->createElement("nomeVist", "");
            $cpf = $dom->createElement("cpf", "");
            $rua = $dom->createElement("rua", "");
            $numero = $dom->createElement("numero", "");
            $complemento = $dom->createElement("complemento", "");
            $bairro = $dom->createElement("bairro", "");
            $cidade = $dom->createElement("cidade", "");
            $estado = $dom->createElement("estado", "");
            $fone = $dom->createElement("fone", "");
            $senha = $dom->createElement("password", "");
            $email = $dom->createElement("email", "");
            $notas = $dom->createElement("notas", "");
            $qtd_vistorias = $dom->createElement("qtd_vistorias", "");      
           
            $vistorianteCreate->appendChild($result);
            $vistorianteCreate->appendChild($id);
            $vistorianteCreate->appendChild($imob_id);
            $vistorianteCreate->appendChild($nomeVist);
            $vistorianteCreate->appendChild($cpf);
            $vistorianteCreate->appendChild($rua);
            $vistorianteCreate->appendChild($numero);
            $vistorianteCreate->appendChild($complemento);
            $vistorianteCreate->appendChild($bairro);
            $vistorianteCreate->appendChild($cidade);
            $vistorianteCreate->appendChild($estado);
            $vistorianteCreate->appendChild($fone);
            $vistorianteCreate->appendChild($senha);
            $vistorianteCreate->appendChild($email);
            $vistorianteCreate->appendChild($notas);
            $vistorianteCreate->appendChild($qtd_vistorias);

            $retornoLogin->appendChild($vistorianteCreate);
            $dom->appendChild($retornoLogin);

            $xml = $dom->saveXML();
            return $xml;
        }
    }

    //desmonta xml de retorno de cadastro da imobiliaria
    public function desmontaXmlLogin($xml) {

        //retira os espaÃ§os da string para que a mesma possa ser serealizada para SimpleXml
        $xmlFormatado = str_replace("\n", "", $xml);
        //monta um new Simplexml de uma string
        $xml = simplexml_load_string($xmlFormatado);
        return $xml;
    }

}
