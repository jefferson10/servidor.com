<?php

class ParserXmlVistoriante {
    public $codificacao = "UTF-8";
    public function desmontaXmlVistorianteCadastro($xml) {

        //retira os espaÃ§os da string para que a mesma possa ser serealizada para SimpleXml
        $xmlFormatado = str_replace("\n", "", $xml);
        //monta um new Simplexml de uma string
        return simplexml_load_string($xmlFormatado);
    }
    
    //monta um xml de retorno, com o resultado da requisiÃ§Ã£o. 1 = true e 0 = false
    public function montaXmlRetornoCadastroVistoriante($result){
       
        $dom = new DOMDocument("1.0", $this->codificacao);
        #retirar os espacos em branco
        $dom->preserveWhiteSpace = false;
        #gerar o codigo
        $dom->formatOutput = true;
        #criando o nÃ³ principal (root)
        $retornoVistoriante = $dom->createElement("returnCadastroVistoriante");
        #nÃ³ filho (usuario)
        $retornoCadastroVistoriante = $dom->createElement("VistorianteCadastro");
        #setanto nomes e atributos dos elementos xml (nÃ³s)
        $resultado = $dom->createElement("resultado", $result); 
        $retornoCadastroVistoriante->appendChild($resultado);
        $retornoVistoriante->appendChild($retornoCadastroVistoriante);
        $dom->appendChild($retornoVistoriante);
            
        $xml = $dom->saveXML();
       //echo $dom->saveXML();
        return $xml;               
    }

    

}
