<?php

class SolicitacaoVistoria extends Eloquent {

    protected $table = 'solicitacoes';
    public $timestamps = false;
    
    
      public function vistoriante() {

        return $this->belongsTo('Vistoriante', 'vist_id');
    }
      public function imovel() {

        return $this->belongsTo('Imovel', 'imov_id');
    }
    public function imobiliaria() {

        return $this->belongsTo('Imobiliaria', 'imob_id');
    }
    
    
    
    //associa dados a uma solicitação corrente
    public function associaDados($dados, $imobId) {
        $this->imob_id = $imobId;
        $this->imov_id = $dados->idImov;
        $this->vist_id = $dados->idVist;
        $this->dataSolicitacao = date("Y/m/d");
    }

}
