<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Imobiliaria extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'imobiliarias';
    public $timestamps = false;
    protected $guarded = array('email','password');
    protected $dates = ['deleted_at'];

    //metodo que retornarÃ¡ o gestor que estÃ¡ associado a imobiliaria corrente
    public function users() {

        return $this->belongsTo('User', 'id');
    }
     public function solicitacao(){

    	return $this->hasOne('SolicitacaoVistoria','imob_id');
    }

    //metodo que retornarÃ¡ os imoveis administrados pela imobiliaria 
    public function imoveis() {

        return $this->hasMany('Imoveis');
    }

    //metodo que retornarÃ¡ os vistoriantes cadastrados pela imobiliaria 
    public function vistoriantes() {

        $this->hasMany('Vistoriantes', 'imob_id');
    }

    /* metodo que associa os dados passados como paramentro, aos atributos
      da instÃ¢ncia corrente do model Imobiliaria, para que o mesmo possa ser
      salvo no banco. */

    public function associaDados($imobiliaria, $user_id) {

        $this->user_id = $user_id;
        $this->nomeImob = (string)$imobiliaria->nomeImob;
        $this->cnpj = (string)$imobiliaria->cnpj;
        $this->rua = (string)$imobiliaria->rua;
        $this->numero = (string)$imobiliaria->numero;
        $this->complemento = (string)$imobiliaria->complemento;
        $this->bairro = (string)$imobiliaria->bairro;
        $this->cidade = (string)$imobiliaria->cidade;
        $this->estado = (string)$imobiliaria->estado;
        $this->fone = (string)$imobiliaria->fone;
        $this->creci = (string)$imobiliaria->creci;
    }
     /* metodo que associa os dados passados como paramentro, aos atributos
      da instÃ¢ncia corrente do model Imobiliaria, para que o mesmo possa ser
      salvo no banco. */

    public function associaDadosEdicao($imobiliaria) {

        
        $this->nomeImob = (string)$imobiliaria->nomeImob;
        $this->cnpj = (string)$imobiliaria->cnpj;
        $this->rua = (string)$imobiliaria->rua;
        $this->numero = (string)$imobiliaria->numero;
        $this->complemento = (string)$imobiliaria->complemento;
        $this->bairro = (string)$imobiliaria->bairro;
        $this->cidade = (string)$imobiliaria->cidade;
        $this->estado = (string)$imobiliaria->estado;
        $this->fone = (string)$imobiliaria->fone;
        $this->creci = (string)$imobiliaria->creci;
    }
  

    /* metodo que associa um item do xml, a uma imobiliaria corrente */

    public function xmlImobiliaria($itemXml, $user_id) {

        $this->user_id = $user_id;
        $this->nomeImob = (string) $itemXml->nomeImob;
        $this->cnpj = (string) $itemXml->cnpj;
        $this->rua = (string) $itemXml->rua;
        $this->numero = (string) $itemXml->numero;
        $this->complemento = (string) $itemXml->complemento;
        $this->bairro = (string) $itemXml->bairro;
        $this->cidade = (string) $itemXml->cidade;
        $this->fone = (string) $itemXml->fone;
        $this->creci = (string) $itemXml->creci;
    }
    

    //metodo que devolve todas as imobiliÃ¡ria, ordenadas pelo o id
    public static function options() {
        return array('' => '') + static::orderBy('id')->lists('nomeImob', 'id');
    }

    //metodo que confere a existÃªncia de uma imobiliaria ja cadastrada com um determinado cnpj
    public static function confereCnpj($cnpj) {
        $resultado = Imobiliaria::where('cnpj', $cnpj)->get()->toArray();
        if($resultado != NULL){
            return 1;
        }else{
            return 0;
        }

        
    }

}
