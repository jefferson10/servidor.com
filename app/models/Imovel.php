<?php

class Imovel extends Eloquent {

    protected $table = 'imoveis';
    public $timestamps = false;

    
     public function solicitacao(){

    	return $this->hasOne('SolicitacaoVistoria','imov_id');
    }
    /* metodo que associa os dados passados como paramentro, aos atributos
      da instÃ¢ncia corrente do model Imobiliaria, para que o mesmo possa ser
      salvo no banco. */

    public function associaDados($imovel) {

        $this->imob_id = (string) $imovel->imob_id;
        $this->rua = (string) $imovel->rua;
        $this->numero = (string) $imovel->numero;
        $this->complemento = (string) $imovel->complemento;
        $this->bairro = (string) $imovel->bairro;
        $this->cidade = (string) $imovel->cidade;
        $this->estado = (string) $imovel->estado;
    }

    /* metodo que devolve todos os imoveis administrados pelo codigo da imobiliaria
      passados com referencia = $codImobiliaria */

    public static function getImoveis($codImobiliaria) {
        $imoveis = Imovel::where('imob_id', $codImobiliaria)
                ->where('solicitacao', 0)
                ->get();


        return $imoveis;
    }
    //muda a situacao do imovel para true
    public function atualizaSituacao($entrada){
        $this->solicitacao = $entrada;
    }

}
