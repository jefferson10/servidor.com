<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vistoria
 *
 * @author jefferson
 */
class VistoriaModel extends Eloquent{
    protected $table = 'vistorias';
    public $timestamps = false;
    
    public function associaDados($idSolicitacao,$vistoria){
        $this->id_solicitacao = $idSolicitacao;
        $this->portas = $vistoria->portas;
        $this->janelas = $vistoria->janelas;
        $this->fechaduras = $vistoria->fechaduras;
        $this->tomadas = $vistoria->tomadas;
        $this->pintura = $vistoria->pintura;
        $this->pisos = $vistoria->pisos;
        $this->torneiras = $vistoria->torneiras;
        $this->luminarias = $vistoria->luminarias;
        $this->pia = $vistoria->pia;
        $this->tanque = $vistoria->tanque;
    }
}
