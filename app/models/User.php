<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public function imobiliaria() {

        return $this->hasOne('Imobiliaria', 'user_id');
    }

    //metodo que confere se ja existe algum usuario cadastrado com email
    public static function confereEmail($email) {
        $resultado = User::where('email', $email)->get()->toArray();
        if ($resultado != NULL) {
            return 1;
        } else {
            return 0;
        }
    }

    /* metodo que associa os dados a instância do model User, para que o mesmo
      possa ser salvo no banco */

    public function associaDados($usuario) {

        $this->email = (string) $usuario->email;
        $this->password = (string) $usuario->senha;
        $this->dataCadastro = date("Y/m/d");
        $this->perfil = 2;
    }

    /* metodo que associa os dados a instância do model User, para que o mesmo
      possa ser salvo no banco */

    public function associaDadosEdicao($usuario) {

        $this->email = (string) $usuario->email;
        $this->password = (string) $usuario->senha;
    }

    //metodo que valida se o usuario tem permissão para consumir o webservice
    public static function validaUsuario($user) {
        $email = Crypt::decrypt((string) $user->email);
        $password = Crypt::decrypt((string) $user->password);

        $user = User::where('email', $email)
                ->where('password', $password)
                ->get();

        if ($user != null) {
            return true;
        } else {
            return false;
        }
    }
    
}
