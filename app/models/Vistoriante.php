<?php

class Vistoriante extends Eloquent {

    protected $table = 'vistoriantes';
    public $timestamps = false;
    protected $guarded = array('email', 'password');
    
     public function solicitacao(){

    	return $this->hasOne('SolicitacaoVistoria','vist_id');
    }

    /* metodo que associa os dados passados como paramentro, aos atributos
      da instÃ¢ncia corrente do model Imobiliaria, para que o mesmo possa ser
      salvo no banco. */

    public function associaDados($entradas, $imob_id) {

        $this->imob_id = $imob_id;
        $this->nomeVist = (string) $entradas->nomeVist;
        $this->cpf = (string) $entradas->cpf;
        $this->rua = (string) $entradas->rua;
        $this->numero = (string) $entradas->numero;
        $this->complemento = (string) $entradas->complemento;
        $this->bairro = (string) $entradas->bairro;
        $this->cidade = (string) $entradas->cidade;
        $this->estado = (string) $entradas->estado;
        $this->fone = (string) $entradas->fone;
        $this->email = (string) $entradas->email;
        $this->password =(string) $entradas->senha;
        $this->dataCadastro = date("Y/m/d");
    }

        //metodo que confere a existÃªncia de um vistoriante ja cadastrado com um determinado cpf
    public static function confereCpf($cpf) {
        $resultado = Vistoriante::where('cpf', $cpf)->get()->toArray();
        if($resultado != NULL){
            return 1;
        }else{
            return 0;
        }

        
    }
        //metodo que confere a existÃªncia de uma imobiliaria ja cadastrada com um determinado cnpj
    public static function confereEmail($email) {
        $resultado = Vistoriante::where('email', $email)->get()->toArray();
        if($resultado != NULL){
            return 1;
        }else{
            return 0;
        }

        
    }

    /* metodo que devolve todos os vistoriantes cadastrados pela imobiliaria que tem o id do dado
      passado com referencia = $codImobiliaria */

    public static function getVistoriantes($codImobiliaria) {
        $vistoriantes = Vistoriante::where('imob_id', $codImobiliaria)
                                   ->orWhere('imob_id','0')->get();
        
        return $vistoriantes;
    }
    public static function validaVistoriante($vistoriante){
        $email = Criptografia::descriptografa($vistoriante->email);
        $password = Criptografia::descriptografa($vistoriante->password);

        $vistoriante = Vistoriante::where('email', $email)
                ->where('password', $password)
                ->get();

        if ($vistoriante != null) {
            return true;
        } else {
            return false;
        }
    }

}
