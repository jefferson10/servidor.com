<?php

class ControladorVistoria extends BaseController {

    //realiza o registro da solicitação
    public function postSolicitacao() {
        $xml = $_POST['xml'];
        $SimpleXml = apoioVistoria::desmontaXml($xml);
        $validacao = User::validaUsuario($SimpleXml);
        if ($validacao) {
            $resultado = apoioVistoria::registraSolicitacao($SimpleXml);
            return apoioVistoria::montaXmlRetornoSolicitacaoVistoria($resultado);
        } else {
            return apoioVistoria::montaXmlRetornoSolicitacaoVistoria('5');
        }
    }

    

    
    public function postVistoria(){
        $xml = $_POST['xml'];
        $SimpleXml = apoioVistoria::desmontaXml($xml);
        $resultado = apoioVistoria::registraVistoria($SimpleXml);
        return apoioVistoria::montaXmlRetornoRegistroVistoria($resultado);
        
    }
    
}
