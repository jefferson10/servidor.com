<?php

class ControladorCadastros extends BaseController {

/////////////////////// Funções Imobiliária ////////////////////////////////////
    /*     * ************************************************************************************ */
//realiza a persistência da imobiliária no sistema

    public function postImobiliaria() {

        $xml = $_POST['xml'];
        $SimpleXml = ControladorCadastros::desmontaXmlImobiliariaCadastro($xml);

        $resultado = ControladorCadastros::confereCnpjEemailImobiliaria($SimpleXml);

        if ($resultado == 2 || $resultado == 3) {
            ob_clean();
            $xmlRetornoCadastro = ControladorCadastros::montaXmlRetornoCadastroImobiliaria($resultado);
            return $xmlRetornoCadastro;
        }

        $userId = ControladorCadastros::cadastraGestorImobiliaria($SimpleXml->usuario);
        if ($userId == 0) {

            ob_clean();
            $xmlRetornoCadastro = ControladorCadastros::montaXmlRetornoCadastroImobiliaria(4);
            return $xmlRetornoCadastro;
        }


        if (ControladorCadastros::cadastraImobiliaria($SimpleXml->imobiliaria, $userId) == 0) {
            $user = User::where('id', $userId)->get()->toArray();
            $user->delete();
            ob_clean();

            $xmlRetornoCadastro = ControladorCadastros::montaXmlRetornoCadastroImobiliaria(5);
            return $xmlRetornoCadastro;
        }
        ob_clean();

        $xmlRetornoCadastro = ControladorCadastros::montaXmlRetornoCadastroImobiliaria(1);
        return $xmlRetornoCadastro;
    }

//método que desmontará o xml, da requisiçãode cadastrar
    public static function desmontaXmlImobiliariaCadastro($xml) {
        $parser = new ParserXmlImobiliaria();
        return $parser->desmontaXmlImobiliariaCadastro($xml);
    }

    //método que desmontará o xml, da requisiçãode cadastrar
    public static function montaXmlRetornoCadastroImobiliaria($resultado) {
        $parser = new ParserXmlImobiliaria();
        return $parser->montaXmlRetornoCadastroImobiliaria($resultado);
    }

//confere se ja existe um mesmo cnpj ou usuario cadastrado
    public static function confereCnpjEemailImobiliaria($SimpleXml) {
        if (ControladorCadastros::confereCnpj($SimpleXml->imobiliaria)) {

            return 2;
        } else if (ControladorCadastros::confereEmailUser($SimpleXml->usuario)) {
            return 3;
        } else {
            return 4;
        }
    }

//metodo que irá conferir se já¡ existe uma imobiliária cadastrada com esse cnpj    
    public static function confereCnpj($imobiliaria) {


        $resultado = Imobiliaria::confereCnpj((string) $imobiliaria->cnpj);

        if ($resultado == 1) {

            return 1;
        } else {

            return 0;
        }
    }

//metodo que irá conferir se já existe um usuario cadastrada com esse email    
    public static function confereEmailUser($usuario) {

        $resultado = User::confereEmail((string) $usuario->email);


        if ($resultado == 1) {

            return 1;
        } else {

            return 0;
        }
    }

    //cadastra imobiliaria
    public static function cadastraImobiliaria($imobiliariaEnt, $user_id) {
        $imobiliaria = new Imobiliaria();
        $imobiliaria->associaDados($imobiliariaEnt, $user_id);
        if ($imobiliaria->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    //cadastra gestorImobiliaria
    public static function cadastraGestorImobiliaria($usuario) {
        $user = new User();
        $user->associaDados($usuario);
        //var_dump($user);

        if ($user->save()) {
            return $user->id;
        } else {
            return 0;
        }
    }

    /////////// Fim das Funções Imobiliária //////////////////////////////
    /*     * *********************************************************************** */

    /////////// Começo das Funções Vistoriante //////////////////////////////
    /*     * *********************************************************************** */
//realiza a persistência do vistoriante no sistema
    public function postVistoriante() {

        $xml = $_POST['xml'];

        $SimpleXml = ControladorCadastros::desmontaXmlVistorianteCadastro($xml);
        $resultadoExistenciaEmailCpf = ControladorCadastros::confereCpfEemailVistoriante($SimpleXml);
        if ($resultadoExistenciaEmailCpf == 2 || $resultadoExistenciaEmailCpf == 3) {
            ob_clean();
            $xmlRetornoCadastro = ControladorCadastros::montaXmlRetornoCadastroVistoriante($resultadoExistenciaEmailCpf);

            return $xmlRetornoCadastro;
        }
        $resultadoCadastro = ControladorCadastros::cadastraVistoriante($SimpleXml->vistoriante, $SimpleXml->usuario);

        ob_clean();
        $xmlRetornoCadastro = ControladorCadastros::montaXmlRetornoCadastroVistoriante($resultadoCadastro);

        return $xmlRetornoCadastro;
    }

    //método que desmontará o xml, da requisiçãode cadastrar
    public static function desmontaXmlVistorianteCadastro($xml) {
        $parser = new ParserXmlVistoriante();
        return $parser->desmontaXmlVistorianteCadastro($xml);
    }

//confere se ja existe um mesmo cpf ou email cadastrado
    public static function confereCpfEemailVistoriante($SimpleXml) {
        if (ControladorCadastros::confereCpfVistoriante($SimpleXml->vistoriante)) {

            return 2;
        } else if (ControladorCadastros::confereEmailVistoriante($SimpleXml->vistoriante)) {
            return 3;
        } else {
            return 4;
        }
    }

//metodo que irá conferir se já¡ existe um vistoriante cadastrado com esse cpf    
    public static function confereCpfVistoriante($vistoriante) {

        $resultado = Vistoriante::confereCpf((string) $vistoriante->cpf);

        if ($resultado == 1) {

            return 1;
        } else {

            return 0;
        }
    }

//metodo que irá conferir se já existe um usuario cadastrado com esse email    
    public static function confereEmailVistoriante($vistoriante) {

        $resultado = Vistoriante::confereEmail((string) $vistoriante->email);
        if ($resultado == 1) {

            return 1;
        } else {

            return 0;
        }
    }

    //cadastra Vistoriante
    public static function cadastraVistoriante($dadosVistoriante, $usuario) {
        $vistoriante = new Vistoriante();

        if ($usuario->usuarioId != '0') {
            if ($imobiliaria = Imobiliaria::where('user_id', $usuario->usuarioId)->get()->first()) {
                $vistoriante->associaDados($dadosVistoriante, $imobiliaria->id);
                if ($vistoriante->save()) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {

            $vistoriante->associaDados($dadosVistoriante, '0');
            if ($vistoriante->save()) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public static function montaXmlRetornoCadastroVistoriante($resultado) {
        $parser = new ParserXmlVistoriante();
        return $parser->montaXmlRetornoCadastroVistoriante($resultado);
    }

    /////////// Fim das Funções Vistoriantes //////////////////////////////
    /*     * *********************************************************************** */
    /////////// Começo das Funções Imovel //////////////////////////////
    /*     * *********************************************************************** */
    //realiza a persistência do Imovel no sistema   


    
    public function postImovel() {

        $xml = $_POST['xml'];
        $SimpleXml = ControladorCadastros::desmontaXmlCadastroImovel($xml);
        $resultadoCadastro = ControladorCadastros::cadastraImovel($SimpleXml);
        if($resultadoCadastro == '1'){
            ob_clean();
            $xml = ControladorCadastros::montaXmlRetornoCadastroImovel($resultadoCadastro);
            return $xml;
        }else{
            ob_clean();
            $xml = ControladorCadastros::montaXmlRetornoCadastroImovel($resultadoCadastro);
            return $xml;
        }
        
    }

    //desmonta o xml da requisicao
    public static function desmontaXmlCadastroImovel($xml) {
        $parser = new ParserXmlImovel();
        return $parser->desmontaXmlImovelCadastro($xml);
    }
    //cadastra o imovel no banco
    public static function cadastraImovel($SimpleXml){
        $imovel = new Imovel();
        $imovel->associaDados($SimpleXml->imovel);
        if($imovel->save()){
            return 1;
        }else{
            return 0;
        }
    }
    //desmonta o xml da requisicao
    public static function montaXmlRetornoCadastroImovel($resultado) {
        $parser = new ParserXmlImovel();
        return $parser->montaXmlRetornoCadastroImovel($resultado);
    }
    

}
