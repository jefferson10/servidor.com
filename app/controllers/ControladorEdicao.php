<?php

class ControladorEdicao extends BaseController {

/////////////////////// Funções Imobiliária ////////////////////////////////////
    /*     * ************************************************************************************ */
//realiza a persistência da imobiliária no sistema

    public function postImobiliaria() {

        $xml = $_POST['xml'];
        $SimpleXml = ControladorEdicao::desmontaXmlImobiliariaEdicao($xml);
        $resultado = ControladorEdicao::editaImobiliaria($SimpleXml);
        ob_clean();
        $xmlRetornoEdicao = ControladorEdicao::montaXmlRetornoEdicaoImobiliaria($resultado);
        return $xmlRetornoEdicao;
    }

//método que desmontará o xml, da requisiçãode cadastrar
    public static function desmontaXmlImobiliariaEdicao($xml) {
        $parser = new ParserXmlImobiliaria();
        return $parser->desmontaXmlImobiliariaCadastro($xml);
    }

    //método que desmontará o xml, da requisiçãode cadastrar
    public static function montaXmlRetornoEdicaoImobiliaria($resultado) {
        $parser = new ParserXmlImobiliaria();
        return $parser->montaXmlRetornoCadastroImobiliaria($resultado);
    }


    //edita imobiliaria e gestor da Imobiliaria
    public static function editaImobiliaria($SimpleXml) {
        
        $user = User::find($SimpleXml->id);
        if($user != null){
            $user->associaDadosEdicao($SimpleXml->usuario);
            $imobiliariaDados = $user->imobiliaria;
            $imobiliaria = Imobiliaria::find($imobiliariaDados->id);
            $imobiliaria->associaDadosEdicao($SimpleXml->imobiliaria); 
            if($user->save()){
                if($imobiliaria->save()){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }else{
            return 2;
        }
        
    }

    /////////// Fim das Funções Imobiliária //////////////////////////////
    /*     * *********************************************************************** */

    /////////// Começo das Funções Vistoriante //////////////////////////////
    /*     * *********************************************************************** */
//realiza a persistência do vistoriante no sistema
    public function postVistoriante() {

        $xml = $_POST['xml'];

        $SimpleXml = ControladorEdicao::desmontaXmlVistorianteCadastro($xml);
        $resultadoExistenciaEmailCpf = ControladorEdicao::confereCpfEemailVistoriante($SimpleXml);
        if ($resultadoExistenciaEmailCpf == 2 || $resultadoExistenciaEmailCpf == 3) {
            ob_clean();
            $xmlRetornoCadastro = ControladorEdicao::montaXmlRetornoCadastroVistoriante($resultadoExistenciaEmailCpf);

            return $xmlRetornoCadastro;
        }
        $resultadoCadastro = ControladorEdicao::cadastraVistoriante($SimpleXml->vistoriante, $SimpleXml->usuario);

        ob_clean();
        $xmlRetornoCadastro = ControladorEdicao::montaXmlRetornoCadastroVistoriante($resultadoCadastro);

        return $xmlRetornoCadastro;
    }

    //método que desmontará o xml, da requisiçãode cadastrar
    public static function desmontaXmlVistorianteCadastro($xml) {
        $parser = new ParserXmlVistoriante();
        return $parser->desmontaXmlVistorianteCadastro($xml);
    }

//confere se ja existe um mesmo cpf ou email cadastrado
    public static function confereCpfEemailVistoriante($SimpleXml) {
        if (ControladorEdicao::confereCpfVistoriante($SimpleXml->vistoriante)) {

            return 2;
        } else if (ControladorEdicao::confereEmailVistoriante($SimpleXml->vistoriante)) {
            return 3;
        } else {
            return 4;
        }
    }

//metodo que irá conferir se já¡ existe um vistoriante cadastrado com esse cpf    
    public static function confereCpfVistoriante($vistoriante) {

        $resultado = Vistoriante::confereCpf((string) $vistoriante->cpf);

        if ($resultado == 1) {

            return 1;
        } else {

            return 0;
        }
    }

//metodo que irá conferir se já existe um usuario cadastrado com esse email    
    public static function confereEmailVistoriante($vistoriante) {

        $resultado = Vistoriante::confereEmail((string) $vistoriante->email);
        if ($resultado == 1) {

            return 1;
        } else {

            return 0;
        }
    }

    //cadastra Vistoriante
    public static function cadastraVistoriante($dadosVistoriante, $usuario) {
        $vistoriante = new Vistoriante();

        if ($usuario->usuarioId != '0') {
            if ($imobiliaria = Imobiliaria::where('user_id', $usuario->usuarioId)->get()->first()) {
                $vistoriante->associaDados($dadosVistoriante, $imobiliaria->id);
                if ($vistoriante->save()) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {

            $vistoriante->associaDados($dadosVistoriante, '0');
            if ($vistoriante->save()) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public static function montaXmlRetornoCadastroVistoriante($resultado) {
        $parser = new ParserXmlVistoriante();
        return $parser->montaXmlRetornoCadastroVistoriante($resultado);
    }

    /////////// Fim das Funções Vistoriantes //////////////////////////////
    /*     * *********************************************************************** */
    /////////// Começo das Funções Imovel //////////////////////////////
    /*     * *********************************************************************** */
    //realiza a persistência do Imovel no sistema   


    
    public function postImovel() {

        $xml = $_POST['xml'];
        $SimpleXml = ControladorEdicao::desmontaXmlCadastroImovel($xml);
        $resultadoCadastro = ControladorEdicao::cadastraImovel($SimpleXml);
        if($resultadoCadastro == '1'){
            ob_clean();
            $xml = ControladorEdicao::montaXmlRetornoCadastroImovel($resultadoCadastro);
            return $xml;
        }else{
            ob_clean();
            $xml = ControladorEdicao::montaXmlRetornoCadastroImovel($resultadoCadastro);
            return $xml;
        }
        
    }

    //desmonta o xml da requisicao
    public static function desmontaXmlCadastroImovel($xml) {
        $parser = new ParserXmlImovel();
        return $parser->desmontaXmlImovelCadastro($xml);
    }
    //cadastra o imovel no banco
    public static function cadastraImovel($SimpleXml){
        $imovel = new Imovel();
        $imovel->associaDados($SimpleXml->imovel);
        if($imovel->save()){
            return 1;
        }else{
            return 0;
        }
    }
    //desmonta o xml da requisicao
    public static function montaXmlRetornoCadastroImovel($resultado) {
        $parser = new ParserXmlImovel();
        return $parser->montaXmlRetornoCadastroImovel($resultado);
    }
    

}
