<?php

class ControladorBuscas extends BaseController {

    public function getTenste() {
        $SimpleXml = "'id'=>'12'";
        $solicitacoes = ControladorBuscas::buscaSolicitacoesVistoriante("12"); //$SimpleXml);
        if ($solicitacoes != null) {
            return ControladorBuscas::montaXmlRetornoBuscaSolicitacoesVistoritante($solicitacoes, "1");
        } else {

            return ControladorBuscas::montaXmlRetornoBuscaSolicitacoesVistoritante(null, "0");
        }
    }

    public function getImoveis() {

        return Imovel::all();
    }

    public function getVistoriantes() {

        return Vistoriante::all();
    }

    public function getUsers() {

        return User::all();
    }

    public function getImobiliarias() {

        return Imobiliaria::all();
    }

    public function getSolicitacoes() {
        return SolicitacaoVistoria::all();
    }
    public function getVistoria(){
        
        return VistoriaModel::all(); 
    }

/////////////////////////////////// IMOVEL ////////////////////////////////
    //busca os imoveis cadastrados por uma determinada imobiliaria
    public function postImoveis() {
        $xml = $_POST['xml'];

        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);

        $validacao = ControladorBuscas::validaUsuario($SimpleXml);

        if ($validacao) {
            $imoveis = ControladorBuscas::obtemImoveis($SimpleXml);
            if ($imoveis != null) {
                ob_clean();
                return ControladorBuscas::montaXmlRetornoBuscaImoveis($imoveis, 1);
            } else {
                ob_clean();
                return ControladorBuscas::montaXmlRetornoBuscaImoveis(null, 0);
            }
        } else {
            ob_clean();
            return ControladorBuscas::montaXmlRetornoBuscaImoveis(null, 2);
        }
    }

    //monta o xml de retorno da busca de imoveis
    public static function montaXmlRetornoBuscaImoveis($imoveis, $resultado) {
        $parser = new ParserXmlBuscas();
        return $parser->montaXmlRetornoBuscaImoveis($imoveis, $resultado);
    }

    //desmonta o xml de requisição da busca 
    public static function desmontaXmlBusca($xml) {
        $parser = new ParserXmlBuscas();
        return $parser->desmontaXmlBusca($xml);
    }

    //busca os imoveis no banco
    public static function obtemImoveis($SimpleXml) {
        $id = $SimpleXml->id;
        $user = User::find($id);
        $imobiliaria = $user->imobiliaria;
        $imoveis = Imovel::getImoveis($imobiliaria->id);
        return $imoveis;
    }

    //busca os dados do imovel solicitado
    public function postImovel() {
        $xml = $_POST['xml'];

        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);

        $validacao = ControladorBuscas::validaUsuario($SimpleXml);
        if ($validacao) {
            $imovel = ControladorBuscas::obtemImovel($SimpleXml->id);
            ob_clean();

            return ControladorBuscas::montaXmlRetornoBuscaImovel($imovel);
        } else {
            ob_clean();
            return ControladorBuscas::montaXmlRetornoBuscaImovel(null);
        }
    }

    //busca os imovel no banco
    public static function obtemImovel($id) {
        $imovel = Imovel::find($id);
        return $imovel;
    }

    //monta o xml de retorno da busca de imovel
    public static function montaXmlRetornoBuscaImovel($imovel) {
        $parser = new ParserXmlBuscas();
        return $parser->montaXmlRetornoBuscaImovel($imovel);
    }

////////////////////////////////// VISTORIAN-TE ////////////////////////////////
//busca os vistoriantes cadastrados por uma determinada imobiliaria
    public function postVistoriantes() {
        $xml = $_POST['xml'];
        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);
        $validacao = ControladorBuscas::validaUsuario($SimpleXml);
        if ($validacao) {
            $vistoriantes = ControladorBuscas::obtemVistoriantes($SimpleXml);
            ob_clean();
            return ControladorBuscas::montaXmlRetornoBuscaVistoriantes($vistoriantes);
        } else {
            ob_clean();

            return ControladorBuscas::montaXmlRetornoBuscaVistoriantes(null);
        }
    }

    //monta o xml de retorno da busca de vistoriantes
    public static function montaXmlRetornoBuscaVistoriantes($vistoriantes) {
        $parser = new ParserXmlBuscas();
        return $parser->montaXmlRetornoBuscaVistoriantes($vistoriantes);
    }

    //busca os vistoriantes no banco
    public static function obtemVistoriantes($SimpleXml) {
        $id = $SimpleXml->id;
        $user = User::find($id);
        $imobiliaria = $user->imobiliaria;
        $vistoriantes = Vistoriante::getVistoriantes($imobiliaria->id);
        return $vistoriantes;
    }

    //busca os dados do vistoriante solicitado
    public function postVistoriante() {
        $xml = $_POST['xml'];

        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);

        $validacao = ControladorBuscas::validaUsuario($SimpleXml);
        if ($validacao) {
            $vistoriante = ControladorBuscas::obtemVistoriante($SimpleXml->id);
            ob_clean();

            return ControladorBuscas::montaXmlRetornoBuscaVistoriante($vistoriante);
        } else {
            ob_clean();
            return ControladorBuscas::montaXmlRetornoBuscaVistoriante(null);
        }
    }

    //busca os vistoriantes no banco
    public static function obtemVistoriante($id) {
        $vistoriante = Vistoriante::find($id);
        return $vistoriante;
    }

    //monta o xml de retorno da busca do vistoriante
    public static function montaXmlRetornoBuscaVistoriante($vistoriante) {
        $parser = new ParserXmlBuscas();
        return $parser->montaXmlRetornoBuscaVistoriante($vistoriante);
    }

    //////////////////////////////////////////Vistoria//////////////////////////////////////////////

    public function postSolicitacoes() {
        $xml = $_POST['xml'];
        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);
        $validacao = ControladorBuscas::validaUsuario($SimpleXml);
        if ($validacao) {
            $solicitacoes = ControladorBuscas::buscaSolicitacoesImobiliaria($SimpleXml);
            if ($solicitacoes != null) {
                $xmlRetorno = ControladorBuscas::montaXmlRetornoSolicitacoes($solicitacoes, '1');
            } else {
                $xmlRetorno = ControladorBuscas::montaXmlRetornoSolicitacoes(null, '0');
            }
        } else {
            $xmlRetorno = ControladorBuscas::montaXmlRetornoSolicitacoes(null, '2');
        }
        return $xmlRetorno;
    }

    //busca as solicitacoes 
    public static function buscaSolicitacoesImobiliaria($SimpleXml) {
        $user = User::find($SimpleXml->id);
        $imobiliaria = $user->imobiliaria;
        $solicitacoes = SolicitacaoVistoria::where('imob_id', $imobiliaria->id)
                        ->orderBy('dataSolicitacao', 'desc')->get();
        return $solicitacoes;
    }

    //monta o xml para retorno
    public static function montaXmlRetornoSolicitacoes($solicitacoes, $resultado) {

        $parser = new ParserXmlBuscas();
        return $parser->montaXmlRetornoBuscaSolicitacoes($solicitacoes, $resultado);
    }

    //////////////////////////////////////////Imobiliaria//////////////////////////////////////////////

    public function postImobiliaria() {
        $xml = $_POST['xml'];
        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);
        $validacao = ControladorBuscas::validaUsuario($SimpleXml);

        if ($validacao) {
            $user = User::find($SimpleXml->id);
            $imobiliaria = $user->imobiliaria;
            $xmlRetorno = ControladorBuscas::montaXmlRetornoImobiliaria($imobiliaria, '1');
        } else {
            $xmlRetorno = ControladorBuscas::montaXmlRetornoImobiliaria(null, '0');
        }
        return $xmlRetorno;
    }

    //monta o xml para retorno busca Imobiliaria
    public static function montaXmlRetornoImobiliaria($imobiliaria, $resultado) {

        $parser = new ParserXmlBuscas();
        return $parser->montaXmlRetornoBuscaImobiliaria($imobiliaria, $resultado);
    }

    public function postSolicitacoesvistoriante() {

        $xml = $_POST['xml'];
        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);
        $vistoriante = $SimpleXml->vistoriante;
        $validacao = ControladorBuscas::validaVistoriante($vistoriante);
        if ($validacao) {
            $solicitacoes = ControladorBuscas::buscaSolicitacoesVistoriante($vistoriante->id);
            if ($solicitacoes != null) {
                return ControladorBuscas::montaXmlRetornoBuscaSolicitacoesVistoritante($solicitacoes, "1");
            } else {

                return ControladorBuscas::montaXmlRetornoBuscaSolicitacoesVistoritante(null, "0");
            }
        } else {
            return ControladorBuscas::montaXmlRetornoBuscaSolicitacoesVistoritante(null, "2");
        }
    }

    public static function buscaSolicitacoesVistoriante($idVist) {
        $solicitacoes = SolicitacaoVistoria::where('vist_id', $idVist)
                        ->where('situacao','0')
                        ->orderBy('dataSolicitacao', 'desc')->get();
        return $solicitacoes;
    }

    public static function montaXmlRetornoBuscaSolicitacoesVistoritante($solicitacoes, $resultado) {
        $parser = new ParserXmlBuscas();
        return $parser->montaXmlRetornoBuscaSolicitacoesVistoriante($solicitacoes, $resultado);
    }

    public static function validaUsuario($user) {
        return User::validaUsuario($user);
    }

    public static function validaVistoriante($vistoriante) {
        return Vistoriante::validaVistoriante($vistoriante);
    }

    public function postImovelvistoriante() {
        $xml = $_POST['xml'];
        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);
        $vistoriante = $SimpleXml->vistoriante;
        $validacao = ControladorBuscas::validaVistoriante($vistoriante);
        if ($validacao) {
            $imovel = ControladorBuscas::buscaImovelVistorianteSolicitacao($vistoriante->idSolicitacao);

            if ($imovel != null) {
                return ControladorBuscas::montaXmlRetornoBuscaImovelSolicitacaoVistoritante($imovel, "1");
            } else {

                return ControladorBuscas::montaXmlRetornoBuscaImovelSolicitacaoVistoritante(null, "0");
            }
        } else {
            return ControladorBuscas::montaXmlRetornoBuscaImovelSolicitacaoVistoritante(null, "2");
        }
    }

    public static function buscaImovelVistorianteSolicitacao($idSolicitacao) {
        $solicitacao = SolicitacaoVistoria::find($idSolicitacao);
        $imovel = $solicitacao->imovel;
        return $imovel;
    }

    public static function montaXmlRetornoBuscaImovelSolicitacaoVistoritante($imovel, $resultado) {
        $parser = new ParserXmlBuscas();
        return $parser->montaXmlRetornoBuscaImovelVistoriante($imovel, $resultado);
    }

    public function getTeste() {
        $xml = '<?xml version="1.0" encoding="ISO-8859-1"?><BuscaSolicitacoes>
    <vistoriante>
    <id>22</id>
    <email>130e7b203d0fd1eb3a8157c60bfc0b441f5b4b82c55ae72dfeb2282b860059ca</email>
    <password>e4774725ad45b19f056638b33365d6b0d31f2a87e4af1a226de0ca736e9f53ec67b3aeecd639e2f4f33f4bf918b96324d22caccfbf451f00bb8114c3567c91b5</password>
    </vistoriante>
    </BuscaSolicitacoes>';
        $SimpleXml = ControladorBuscas::desmontaXmlBusca($xml);

        $vistoriante = $SimpleXml->vistoriante;

        $validacao = ControladorBuscas::validaVistoriante($vistoriante);

        if ($validacao) {
            $solicitacoes = ControladorBuscas::buscaSolicitacoesVistoriante($SimpleXml);
            if ($solicitacoes != null) {
                return ControladorBuscas::montaXmlRetornoBuscaSolicitacoesVistoritante($solicitacoes, "1");
            } else {

                return ControladorBuscas::montaXmlRetornoBuscaSolicitacoesVistoritante(null, "0");
            }
        } else {
            return ControladorBuscas::montaXmlRetornoBuscaSolicitacoesVistoritante(null, "2");
        }
    }

}
