<?php

class ControladorLogin extends BaseController {

    public function getTeste() {

        
        $api = new ApiCrypter();
        $v = Crypt::encrypt("123456");;
        echo "antes".$v;
        
        $t= Crypt::decrypt($v);
        echo "depois".$t;
                
//        ob_clean();
  //      return Crypt::decrypt($j);
        /*$parser = new ParserXmlLogin();
        
        $auth = ControladorLogin::autenticacaoVistoriante("gustavo@gmail.com","123456");
        
        var_dump($parser->montaXmlRetornoLoginVistoriante($auth['vistoriante'], $auth['resultado']));*/
    }

    //realiza o login
    public function postLogin() {
        $xml = $_POST['xml'];
        $SimpleXml = ControladorLogin::desmontaXmlLogin($xml);
        $login = $SimpleXml->login;
        
        $usuario = Crypt::decrypt($login->email);
        $senha = Crypt::decrypt($login->password);
        
        $auth = ControladorLogin::autenticacao($usuario, $senha);
        ob_clean();
        $xmlRetorno = ControladorLogin::montaXmlRetornoLogin($auth['user'], $auth['resultado']);
        return $xmlRetorno;
    }

    public function postVistoriante() {
        $xml = $_POST['xml'];
        $parser = new ParserXmlLogin();
        $SimpleXml = $parser->desmontaXmlLogin($xml);
        $vistoriante = $SimpleXml->vistoriante;
        $email = Criptografia::descriptografaJson($vistoriante->email);
        $password = Criptografia::descriptografaJson($vistoriante->password);
        $auth = ControladorLogin::autenticacaoVistoriante($email, $password);
        ob_clean();                 
       return $parser->montaXmlRetornoLoginVistoriante($auth['vistoriante'], $auth['resultado']);
//     return Vistoriante::all();
        //return $auth;
        
    }

    //desmonta xml de Login
    public static function desmontaXmlLogin($xml) {
        $parser = new ParserXmlLogin();
        return $parser->desmontaXmlLogin($xml);
    }

    //monta xml de Login
    public static function montaXmlRetornoLogin($user, $resultado) {
        $parser = new ParserXmlLogin();
        return $parser->montaXmlRetornoLogin($user, $resultado);
    }

    //realiza a autenticação do usuário
    public static function autenticacao($usuario, $senha) {
        $user = ControladorLogin::buscaUsuarioBanco($usuario);

        if ($user != null) {
            if (Hash::check($senha, $user->password)) {
                $resultado = 1;
            } else {
                
                $resultado = 0;
            }
        } else {
            $resultado = 2;
        }
        $retorno = array('user' => $user, 'resultado' => $resultado);
        return $retorno;
    }

    //verifica a existência do usuario no banco
    public static function buscaUsuarioBanco($usuario) {
        $user = DB::table('users')
                ->where('email', '=', $usuario)
                ->where('ativo', '=', 1)
                ->first();

        return $user;
    }

    //realiza a autenticação do usuário
    public static function autenticacaoVistoriante($email, $senha) {
        $vistoriante = ControladorLogin::buscaVistorianteBanco($email);

        if ($vistoriante != null) {
            if (Hash::check($senha, $vistoriante->password)) {
                $resultado = 1;
            } else {
                $vistoriante = null;
                $resultado = 0;
            }
        } else {
            $resultado = 2;
        }
        $retorno = array('vistoriante' => $vistoriante, 'resultado' => $resultado);
        return $retorno;
    }

    //verifica a existência do vistoriante no banco
    public static function buscaVistorianteBanco($email) {
        $vistoriante = DB::table('vistoriantes')
                ->where('email', '=', $email)
                ->where('ativo', '=', 1)
                ->first();

        return $vistoriante;
    }

}
