
<!DOCTYPE HTML>
<!--
        Escape Velocity by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>GVI-(Gestor de Vistorias Imobiliárias)</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="css/style.css" />
        <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
    </head>
    <body class="homepage">


        <div id="page-wrapper">


            <!-- Header -->
            <div id="header-wrapper" class="wrapper">

                <div id="header">


                    @section('logo')
                    <!-- Logo -->
                    <div id="logo">
                        <h1><a href="index.html">Uma nova solução em vistorias.</a></h1>
                        <p>Confira nossos serviços.</p>
                    </div>
                    @show
                    @section('menu')
                    <!-- Nav -->
                    <!-- Nav -->
                    <nav id="nav">
                        <ul>
                            <li class="current"><a href="index.html">Home</a></li>
                            <li>
                                <a href="#">Quem somos</a>
                                <ul>
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Magna veroeros</a></li>
                                    <li><a href="#">Etiam nisl</a></li>
                                    <li>
                                        <a href="#">Sed consequat</a>
                                        <ul>
                                            <li><a href="#">Lorem dolor</a></li>
                                            <li><a href="#">Amet consequat</a></li>
                                            <li><a href="#">Magna phasellus</a></li>
                                            <li><a href="#">Etiam nisl</a></li>
                                            <li><a href="#">Sed feugiat</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Nisl tempus</a></li>
                                </ul>
                            </li>
                            <li><a href="left-sidebar.html">Trabalhe conosco</a>
                                <ul>
                                    <li><a href="#">Seja um vistorian-te</a></li>
                                </ul>
                            </li>
                            <li><a href="right-sidebar.html">Serviços</a>
                                <ul>
                                    <li><a href="#">Portal Web</a></li>
                                    <li><a href="#">Aplicativo Móvel</a></li>
                                </ul>
                            </li>
                            <li><a href="right-sidebar.html">Login</a></li>
                            <li><a href="{{ URL::action('ControladorCadastros@getImobiliaria') }}">Seja um cliente</a></li>

                        </ul>
                    </nav>

                    @show

                </div>

                @section('conteudoLivre')
                <!-- Intro -->
                <div id="intro-wrapper" class="wrapper style1">
                    <div class="title">Introduçao</div>
                    <section id="intro" class="container">
                        <p class="style1">O sistema dará suporte a vistorias mais agéis, usando dispositivos móveis</p>
                        <p class="style2">
                            Escape Velocity is a free responsive<br class="mobile-hide" />
                            site template by <a href="http://html5up.net" class="nobr">HTML5 UP</a>
                        </p>
                        <p class="style3">It's <strong>responsive</strong>, built on <strong>HTML5</strong> and <strong>CSS3</strong>, and released for
                            free under the <a href="http://html5up.net/license">Creative Commons Attribution 3.0 license</a>, so use it for any of
                            your personal or commercial projects &ndash; just be sure to credit us!</p>
                        <ul class="actions">
                            <li><a href="#" class="button style3 big">Proceed</a></li>
                        </ul>
                    </section>
                </div>

                <!-- Main -->
                <div class="wrapper style2">
                    <div class="title">Detalhes</div>
                    <div id="main" class="container">

                        <!-- Image -->
                        <a href="#" class="image featured">
                            <img src="images/pic14.jpg" alt="" />
                        </a>

                        <!-- Features -->
                        <section id="features">

                            <div class="feature-list">
                                <div class="row">
                                    <div class="6u 12u(mobile)">
                                        <section>
                                            <h3 class="icon fa-comment">Apoio técnico de nossa equipe</h3>
                                            <p>A nossa equipe de suporte, estará sempre pronta, para tirar suas dúvidas e lhe dar as melhores soluções .</p>
                                        </section>
                                    </div>
                                    <div class="6u 12u(mobile)">
                                        <section>
                                            <h3 class="icon fa-refresh">Sincronização e manutenção dos dados</h3>
                                            <p>Os dados estarão a disposição do gestor da imobilária com maior agilidade e organização.</p>
                                        </section>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="6u 12u(mobile)">
                                        <section>
                                            <h3 class="icon fa-cog">Portal de administração</h3>
                                            <p>Um portal cheio de funcionalidades, para que o gestor da imobiliária possa gerenciar todas vistorias.</p>
                                        </section>
                                    </div>
                                    <div class="6u 12u(mobile)">
                                        <section>
                                            <h3 class="icon fa-wrench">Adiministração do Portal</h3>
                                            <p> - Realize cadastro de imóveis e vistorian-tes</p>
                                            <p>- Solicite vistorias</p>
                                            <p>- Avalie vistorias</p>
                                            <p> - Emissão de relatório análiticos.</p>
                                        </section>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="6u 12u(mobile)">
                                        <section>
                                            <h3 class="icon fa-check">Vistorias realizadas</h3>
                                            <p>Você será um colaborador, pois após visualisar a vistoria, o mesmo à avaliára; aonde sua nota servirá para uma futura escolha do vistorian-te.</p>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <ul class="actions actions-centered">
                                <li><a href="#" class="button style1 big">Get Started</a></li>
                                <li><a href="#" class="button style2 big">More Info</a></li>
                            </ul>
                        </section>

                    </div>
                </div>

                <!-- Highlights -->
                <div class="wrapper style3">
                    <div class="title">The Endorsements</div>
                    <div id="highlights" class="container">
                        <div class="row 150%">
                            <div class="4u 12u(mobile)">
                                <section class="highlight">
                                    <a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
                                    <h3><a href="#">Aliquam diam consequat</a></h3>
                                    <p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
                                    <ul class="actions">
                                        <li><a href="#" class="button style1">Learn More</a></li>
                                    </ul>
                                </section>
                            </div>
                            <div class="4u 12u(mobile)">
                                <section class="highlight">
                                    <a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
                                    <h3><a href="#">Nisl adipiscing sed lorem</a></h3>
                                    <p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
                                    <ul class="actions">
                                        <li><a href="#" class="button style1">Learn More</a></li>
                                    </ul>
                                </section>
                            </div>
                            <div class="4u 12u(mobile)">
                                <section class="highlight">
                                    <a href="#" class="image featured"><img src="images/pic04.jpg" alt="" /></a>
                                    <h3><a href="#">Mattis tempus lorem</a></h3>
                                    <p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
                                    <ul class="actions">
                                        <li><a href="#" class="button style1">Learn More</a></li>
                                    </ul>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                @show
                <!-- Footer -->
                <div id="footer-wrapper" class="wrapper">
                    <div class="title">Contatos</div>
                    <div id="footer" class="container">
                        <header class="style1">
                            <h2>Entre em contato, será um enorme prazer em atendê-lo</h2>
                            <p>
                                Poupe tempo, envie nos uma mensagem.<br />

                            </p>
                        </header>
                        <hr />
                        <div class="row 150%">
                            <div class="6u 12u(mobile)">

                                <!-- Contact Form -->
                                <section>
                                    <form method="post" action="#">
                                        <div class="row 50%">
                                            <div class="6u 12u(mobile)">
                                                <input type="text" name="name" id="contact-name" placeholder="Nome" />
                                            </div>
                                            <div class="6u 12u(mobile)">
                                                <input type="text" name="email" id="contact-email" placeholder="Email" />
                                            </div>
                                        </div>
                                        <div class="row 50%">
                                            <div class="12u">
                                                <textarea name="message" id="contact-message" placeholder="Mensagem" rows="4"></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="12u">
                                                <ul class="actions">
                                                    <li><input type="submit" class="style1" value="Enviar" /></li>
                                                    <li><input type="reset" class="style2" value="Limpar" /></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </form>
                                </section>

                            </div>
                            <div class="6u 12u(mobile)">

                                <!-- Contact -->
                                <section class="feature-list small">
                                    <div class="row">
                                        <div class="6u 12u(mobile)">
                                            <section>
                                                <h3 class="icon fa-home">Endereço</h3>
                                                <p>
                                                    Rua Alencar<br />
                                                    1234 Sobrado<br />
                                                    Poços de Caldas, CEP 37700-000
                                                </p>
                                            </section>
                                        </div>
                                        <div class="6u 12u(mobile)">
                                            <section>
                                                <h3 class="icon fa-comment">Social</h3>
                                                <p>
                                                    <a href="https://www.facebook.com/jefferson.pereira.3517">facebook.com/</a>
                                                </p>
                                            </section>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="6u 12u(mobile)">
                                            <section>
                                                <h3 class="icon fa-envelope">Email</h3>
                                                <p>
                                                    <a href="#">jeffersonpuc@gmail.com</a>
                                                </p>
                                            </section>
                                        </div>
                                        <div class="6u 12u(mobile)">
                                            <section>
                                                <h3 class="icon fa-phone">Telefone</h3>
                                                <p>
                                                    (35) 8836-9860
                                                </p>
                                            </section>
                                        </div>
                                    </div>
                                </section>

                            </div>
                        </div>
                        <hr />
                    </div>
                    <div id="copyright">
                        <ul>
                            <li>&copy; Jefferson</li>
                        </ul>
                    </div>
                </div>

            </div>

            <!-- Scripts -->

            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/skel-viewport.min.js"></script>
            <script src="assets/js/util.js"></script>
            <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
            <script src="assets/js/main.js"></script>

    </body>
</html>

