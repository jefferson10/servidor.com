<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Criptografia
 *
 * @author jefferson
 */
class Criptografia {
    public static function criptografa($dado){
        $apiCrypter = new ApiCrypter();
        return $apiCrypter->encrypt($dado);
    }
    public static function descriptografaJson($dado){
        $apiCrypter = new ApiCrypter();
        $dadoComposto = $apiCrypter->decrypt($dado);
        $obj = json_decode($dadoComposto);
        return $obj->data;
    }
    public static function descriptografa($dado){
        $apiCrypter = new ApiCrypter();
        $dadoComposto = $apiCrypter->decrypt($dado);
        return $dadoComposto;
    }
    
}


