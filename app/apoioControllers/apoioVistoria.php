<?php
class apoioVistoria {

    //monta o xml da solicitação da vistoria
    public static function montaXmlRetornoSolicitacaoVistoria($resultado) {
        $parser = new ParserXmlSolicitacaoVistoria();
        return $parser->montaXmlRetornoSoliciatacaoVistoria($resultado);
    }

    //registra a solicitação no banco
    public static function registraSolicitacao($SimpleXml) {
        $user = User::find($SimpleXml->idUser);

        $imobiliaria = $user->imobiliaria;
        if ($imobiliaria != null) {
            $solicitacao = new SolicitacaoVistoria();
            $solicitacao->associaDados($SimpleXml, $imobiliaria->id);
            if ($solicitacao->save()) {
                $imovel = Imovel::find($SimpleXml->idImov);
                $imovel->atualizaSituacao(1);
                if ($imovel->save()) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 3;
            }
        } else {
            return 4;
        }
    }

    public static function registraVistoria($SimpleXml) {
        $idSolicitacao = $SimpleXml->idSolicitacao;
        $vistoria = $SimpleXml->vistoria;
        $vistoriante = $SimpleXml->vistoriante;
        $validacao = Vistoriante::validaVistoriante($vistoriante);
        if ($validacao) {
            $resultado = apoioVistoria::atualizaSolicitacao($idSolicitacao);
            if ($resultado == 4) {
                $vistoriaModel = new VistoriaModel();
                $vistoriaModel->associaDados($idSolicitacao, $vistoria);
                if ($vistoriaModel->save()) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return $resultado;
            }
        } else {
            return 3;
        }
    }

    public static function atualizaSolicitacao($idSolicitacao) {
        $solicitacao = SolicitacaoVistoria::find($idSolicitacao);
        if ($solicitacao != null) {
            $solicitacao->situacao = 1;
            if ($solicitacao->save()) {
                return 4;
            } else {
                return 5;
            }
        } else {
            return 6;
        }
    }

    //desmonta o xml vindo com a requisição
    public static function desmontaXml($xml) {
        $parser = new ParserXmlSolicitacaoVistoria();
        return $parser->desmontaXml($xml);
    }

    public static function montaXmlRetornoRegistroVistoria($resultado) {
        $parser = new ParserXmlVistoria();
        return $parser->montaXmlRetornoRegistroVistoria($resultado);
    }

}
